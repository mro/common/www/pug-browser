const
  path = require('path'),
  webpack = require('webpack'),
  { set } = require('lodash');

module.exports = {
  mode: 'development',
  entry: path.resolve('./index.js'),
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          extends: path.join(__dirname, '.babelrc')
        }
      }
    ]
  },
  resolve: {
    fallback: {
        path: false, fs: false, os: false
    }
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'inline-cheap-module-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: [ 'buffer', 'Buffer' ]
    }),
  ]
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = 'source-map';
  module.exports.mode = 'production';
  set(module.exports, 'optimization.nodeEnv', 'production');
  set(module.exports, 'optimization.moduleIds', 'named');
}
